# Java Executable Parent POM #

This is the parent pom for all BYU HBLL non-web java applications including CLI, cron, and daemon applications. Executables are based in Spring Boot and this POM base dependencies and a build process that produces a single jar containing all the application dependencies.

This POM can be referenced by adding the following to an existing POM:

```xml
<parent>
  <groupId>edu.byu.hbll.maven</groupId>
  <artifactId>executable-parent</artifactId>
  <version>4.0.1</version>
</parent>
```

A child project can be built by running the following command:

    mvn clean package

This will create a file with the name `{artifactId}.jar` in the target directory.
