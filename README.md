# Maven Infrastructure #
A multi-module maven project providing base infrastructure for the BYU Library's Java projects. All Java projects should use the parent poms provided here and the archetype's as guides for structuring projects.

## Reference ##
The following is a quick-reference guide for each component. More information available at the links.

#### [ Library Parent POM ](library-parent/) ####
Add the following to an existing POM:

```xml
<parent>
  <groupId>edu.byu.hbll.maven</groupId>
  <artifactId>library-parent</artifactId>
  <version>4.0.1</version>
</parent>
```

#### [ Executable Parent POM ](executable-parent/) ####
Add the following to an existing POM:

```xml
<parent>
  <groupId>edu.byu.hbll.maven</groupId>
  <artifactId>executable-parent</artifactId>
  <version>4.0.1</version>
</parent>
```

#### [ Application Parent POM ](application-parent/) ####
Add the following to an existing POM:

```xml
<parent>
  <groupId>edu.byu.hbll.maven</groupId>
  <artifactId>application-parent</artifactId>
  <version>4.0.1</version>
</parent>
```

#### [ Library Archetype ](library-archetype/) ####
Execute the following from the command line to invoke the archetype:

    mvn archetype:generate -DarchetypeGroupId=edu.byu.hbll.maven -DarchetypeArtifactId=library-archetype -DarchetypeVersion=4.0.1

#### [ Executable Archetype ](executable-archetype/) ####
Execute the following from the command line to invoke the archetype:

    mvn archetype:generate -DarchetypeGroupId=edu.byu.hbll.maven -DarchetypeArtifactId=executable-archetype -DarchetypeVersion=4.0.1

#### [ Application Archetype ](application-archetype/) ####
Execute the following from the command line to invoke the archetype:

    mvn archetype:generate -DarchetypeGroupId=edu.byu.hbll.maven -DarchetypeArtifactId=application-archetype -DarchetypeVersion=4.0.1

## Java Version
The parent POM specifies the Java version for all child poms. To override the java version in a child pom, add this to your pom.xml.

```xml
<properties>
  <java.version>1.8</java.version>
</properties>
```
