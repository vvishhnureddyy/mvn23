# Executable Archetype #

The Executable Archetype is a Maven archetype that creates non-web applications including CLI, cron, or daemon applications. While the default values are intended for use by BYU HBLL developers, this archetype can be used by any organization or developer by overriding the defaults during project creation. 

## Creating a New Project

Execute the following command from the command line to invoke the archetype:

    mvn archetype:generate -DarchetypeGroupId=edu.byu.hbll.maven -DarchetypeArtifactId=executable-archetype -DarchetypeVersion=4.0.1

You'll be prompted to enter the following:

* `groupId`: The Maven group ID for your project. Defaults to "edu.byu.hbll".
* `artifactId`: The Maven artifact ID, which is generally the name of your project.
* `version`: The Maven version.  Defaults to "1.0.0".
* `package`: The base package name of your project. Defaults to "`groupId`.`artifactId`" (e.g.: edu.byu.hbll.myproject), but should be changed when the artifactId contains invalid characters (such as hyphens) or if you have another reason for it not to be the same.
* `description`: A brief description of the project.

## Running Project Tests

To run the unit and integration tests for this project, run the following command from the project directory:

    mvn verify
    
The Parent POM referenced in this archetype includes support for EclEmma's jacoco-maven-plugin, which can be used to approximate code coverage in your tests.  To display a summary of code coverage, run the following commands after running `mvn verify`:

    # This string is a Base 64 encoded awk instruction used to parse JaCoCo's generated report file.
    JACOCO_PARSER="ewogIGluc3RydWN0aW9ucyArPSAkNCArICQ1OwogIGNvdmVyZWQgKz0gJDUKfQpFTkQgewogIHByaW50IGNvdmVyZWQsICIvIiwgaW5zdHJ1Y3Rpb25zLCAiaW5zdHJ1Y3Rpb25zIGNvdmVyZWQiOwogIHByaW50ICJjb2RlIGNvdmVyYWdlOiIsIDEwMCpjb3ZlcmVkL2luc3RydWN0aW9ucywgIiUiCn0K"
    echo $JACOCO_PARSER | base64 -d >/tmp/jacoco.awk && awk -F"," -f /tmp/jacoco.awk target/site/jacoco/jacoco.csv

## Running a Project

To compile and run the resulting application, run the following command from the project directory:

    mvn package
    java -jar target/ARTIFACT_ID.jar

## Containerizing a Project

The project contains a Dockerfile for building as a Docker image. To build a new image, run the following command from the project directory:

    mvn package
    docker build .

## Logging

Logging is optimized for the CLI use case (ie, very little logging). For cron jobs or daemons, you can make changes to src/main/resources/application.yml:spring.main or src/main/resources/logback.xml to increase logging output. You can also increase logging at runtime by setting appropriate environment variables such as:

    LOGGING_LEVEL_{PACKAGE}=INFO
    SPRING_MAIN_LOG_STARTUP_INFO=TRUE
