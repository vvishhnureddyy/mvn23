# Application Parent POM #

This is the parent pom for all BYU HBLL web applications. This is based on Spring Boot.

This POM can be referenced by adding the following to an existing POM:

```xml
<parent>
  <groupId>edu.byu.hbll.maven</groupId>
  <artifactId>application-parent</artifactId>
  <version>4.0.1</version>
</parent>
```

## Keycloak
Keycloak is included in the parent pom and enabled by default. To disable it, include the following in the application.yml or set an environment variable.

```yaml
keycloak:
  enabled: false
```

```
KEYCLOAK_ENABLED=false
```

To configure it, add something like this to your application.yml.

```yaml
keycloak:
  auth-server-url: https://KEYCLOAK_HOST/auth
  realm: REALM_NAME
  resource: CLIENT_NAME
  public-client: true
  security-constraints:
    - authRoles: [offline_access]
      securityCollections:
        - patterns: ["/*"]
```
